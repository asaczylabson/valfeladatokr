rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 #ismétlések száma
y<-rep(0,ISM) #rajzolashoz
N<-10
URNA<-1:N
K<-0

i<-0
while(i<ISM){
  i<-i+1
  huzas<-sample(URNA,N,replace=FALSE)
  huzas<-c(huzas,huzas[1])
  jo<-1
  j<-0
  while(j<N&&jo>0){
    j<-j+1
    if((huzas[j]==1&&huzas[j+1]==N)||
      (huzas[j]==N&&huzas[j+1]==1)){
      jo<-0
    }
  }
  if(jo==1){K<-K+1}
  y[i]<-1-K/i # komplementer!
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",ISM-K,ISM,1-K/ISM),quote=0)
plot(y,type="p",ylim=c(0,1))
#x<-50:ISM
#print(lsfit(x,y[x])$coefficients)
cat("elm -------------------->\n")
source("nfakt.r")
K=2*nfakt(N-2) # jobbra vagy balra van az N az 1-től
N=nfakt(N-1)
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
