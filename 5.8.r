rm(list=ls())

cat("sim--------------------\n")
ISM<-2000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet

K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  r<-sort(runif(2))
  if(r[2]-r[1]<0.5){# a messze levőket nem veszi figyelembe
    i<-i+1
    # if(r[2]<0.5){# 0 a kijelölt pont
    #   K<-K+1
    # }
    if(r[1]>0.5){# 1 a kijelölt pont
      K<-K+1
    }
    sim[i]<-K/i
  }
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
K<-0.25 # ábrázoljuk x<y illetve y<x esetek szétválasztásával
N<-0.75 
print(sprintf("K=%f, N=%f, K/N=%f",K,N,K/N),quote=0)
library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(K/N,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # basic graphical object
g<-g+geom_line(aes(y=elm), colour="red") # first layer
g<-g+geom_line(aes(y=sim), colour="green")  # second layer
plot(g)
