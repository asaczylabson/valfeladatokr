rm(list=ls())

cat("sim--------------------\n")
ISM<-100000 # ismétlések száma, 
# ennyiszer végezzük el a kísérletet
URNA<-4 # az urnában levő dolgok száma
HANYSZOR<-9 # ennyiszer húzunk
K<-0 # kedvező esetek száma
CEL<-rep(2,URNA)
for(i in 1:ISM){
  huzas<-sample.int(URNA,HANYSZOR,replace=TRUE)
  akt<-rep(0,URNA)
  for(j in 1:HANYSZOR){
    akt[huzas[j]]<-akt[huzas[j]]+1
  }
  if(sum(akt>=CEL)>=URNA){K<-K+1}
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
source("nfakt.r")
K<-(4*nfakt(9))/48 #4* 9!/(2!2!2!3!)
N<-4^9 # mindelépésben mindent választhatok
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
