rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 # ismétlések száma
y<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
JOBB<-4 # 0
BAL<-8 # 1
URNA<-c(rep(0,JOBB),c(1,BAL))
print(URNA)
HUZ<-4
K<-0

i<-0
while(i<ISM){
  i<-i+1
  huzas<-sample(URNA,HUZ,replace=FALSE)
  jo<-1
  j<-0
  while(j<HUZ&&jo>0){
    j<-j+1
    if(huzas[j]==1){jo<-0}
  }
  if(jo>0){K<-K+1}
  y[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
plot(y,type="p",ylim=c(0,1))

cat("elm -------------------->\n")
K<-choose(PAKLI-PAKLI/SZIN,HUZ) # kirakjuk a zöldeket
N<-choose(PAKLI,HUZ) 
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
