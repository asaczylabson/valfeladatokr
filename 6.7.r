rm(list=ls())

cat("sim--------------------\n")
ISM<-10000 # ismétlések száma (ennyiszer végezzük el a kísérletet) 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok

K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  while(1){
    x<-runif(1)
    if(x<0.2){K<-K+1;i<-i+1;break;}
    y<-runif(1)
    if(y<0.3){i<-i+1;break;}
  }
  sim[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
#figyeljük meg hogy a sorrendet figyelembe vettük
p<-0.2*(1/(1-0.8*0.7)) # mértani sor(+függetlenség) 
print(sprintf("p=%f",p),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(p,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # base
g<-g+geom_line(aes(y=elm), colour="red") # layer 1
g<-g+geom_line(aes(y=sim), colour="green")  # layer 2
plot(g)


