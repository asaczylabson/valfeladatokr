rm(list=ls())

cat("sim--------------------\n")
ISM<-10000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet

K<-0 # kedvező esetek száma
for(i in 1:ISM){
  r<-sort(runif(2))
  a<-r[1]
  b<-r[2]-r[1]
  c<-1-r[2]
  r<-sort(c(a,b,c))
    
  if(r[1]+r[2]>r[3]){
    K<-K+1
  }
  sim[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
K<-0.25 # fel kell írni a háromszög-egyenlőtlenségeket
N<-1 
print(sprintf("K=%f, N=%f, K/N=%f",K,N,K/N),quote=0)
library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(K/N,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # basic graphical object
g<-g+geom_line(aes(y=elm), colour="red") # first layer
g<-g+geom_line(aes(y=sim), colour="green")  # second layer
plot(g)
