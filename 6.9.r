rm(list=ls())

cat("sim--------------------\n")
ISM<-10000 # ismétlések száma (ennyiszer végezzük el a kísérletet) 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok

K<-0 # kedvező esetek száma
i<-0
m<-4 # ennyiszer húzunk visszatevéssel
while(i<ISM){
  i<-i+1
  r<-sample.int(10,m,replace = TRUE)
  if(sum(r%%2)<m){K<-K+1}
  sim[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
#figyeljük meg hogy a sorrendet figyelembe vettük
p<-0.9 # ettől kellene nagyobb (treshold) 
print(sprintf("p=%f",p),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(p,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # base
g<-g+geom_line(aes(y=elm), colour="red") # layer 1
g<-g+geom_line(aes(y=sim), colour="green")  # layer 2
plot(g)


