rm(list=ls())

cat("sim--------------------\n")
ISM<-1000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet
URNA<-rep(c(0,1),5) #5 0=piros és 5 1=fehér
print(URNA)
K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  r<-sample(URNA,3,replace=FALSE) # 
  if(r[1]==r[2]){
    i<-i+1
    if(r[3]==0){
      K<-K+1
    }
    sim[i]<-K/i
  }
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
#figyeljük meg hogy a sorrendet figyelembe vettük
K<-5*4*3+5*4*5 # PPP + FFP
N<-2*5*4*8 # első kettő egyforma
print(sprintf("K=%f, N=%f, K/N=%f",K,N,K/N),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(K/N,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # base
g<-g+geom_line(aes(y=elm), colour="red") # layer 1
g<-g+geom_line(aes(y=sim), colour="green")  # layer 2
plot(g)


