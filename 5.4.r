rm(list=ls())

cat("sim--------------------\n")
ISM<-2000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet

K<-0 # kedvező esetek száma
i<-1
while(i<=ISM){
  r<-sample(6,2,replace=TRUE)
  if(r[1]+r[2]%%2 == 1){# csak a páratlanokon belülit nézi
    if(r[1]+r[2]==7){
      K<-K+1
    }
    sim[i]<-K/i
    i<-i+1
  }
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
K<-6 # könnyű számolni mert A\subset B, (1,6)...(6,1)
N<-18 # szimmetria 
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(K/N,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # basic graphical object
g<-g+geom_line(aes(y=elm), colour="red") # first layer
g<-g+geom_line(aes(y=sim), colour="green")  # second layer
plot(g)


