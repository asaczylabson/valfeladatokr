rm(list=ls())

cat("sim -------------------->\n")
ISM<-10000 # ismétlések száma, 
# ennyiszer végezzük el a kísérletet
URNA<-2 # az urnában levő dolgok száma
HANYSZOR<-10 # ennyiszer húzunk
K<-0 # kedvező esetek száma
for(i in 1:ISM){
  huzas<-sample.int(URNA,HANYSZOR,replace=TRUE)
  sh<-sum(huzas)
  if(sh==HANYSZOR||sh==2*HANYSZOR){
    K<-K+1
  }
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm -------------------->\n")
K<-2
N<-URNA^HANYSZOR
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)