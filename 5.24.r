rm(list=ls())

cat("sim--------------------\n")
ISM<-10000 # ismétlések száma (ennyiszer végezzük el a kísérletet) 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# hard-wired
N1<-100;
N2<-150
N3<-250
#URNA1 hibas-nem hibás= 1-0, tíznapi termelésnek megfelelően
URNA1<-c(rep(1,3),rep(0,N1-3),rep(1,9),rep(0,N2-9),rep(1,5),rep(0,N3-5))
#URNA2 a gép sorszáma, a termelés nagyságának megfelelő számban
URNA2<-c(rep(1,100),rep(2,150),rep(3,250));
K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  r<-sample.int(N1+N2+N3,1)
  if(URNA1[r]==1){#rossz,hibás
    i<-i+1
    if(URNA2[r]==1){K<-K+1}
  }
  sim[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
#figyeljük meg hogy a sorrendet figyelembe vettük
p<-3/17 # 0.3/(0.3+0.9+0.5), Bayes, vagy 1xűen kedvező per összes 
print(sprintf("p=%f",p),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(p,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # base
g<-g+geom_line(aes(y=elm), colour="red") # layer 1
g<-g+geom_line(aes(y=sim), colour="green")  # layer 2
plot(g)


