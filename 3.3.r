rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 # ismétlések száma
KO<-6 # "kocka" oldalainak a száma
KSZ<-2 # kockák száma
K<-0 # kedvező esetek száma
for(i in 1:ISM){
  dobas<-sample.int(KO,KSZ,replace=TRUE)
  if(dobas[1]<dobas[2]){
    K<-K+1
  }
  
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm -------------------->\n")
K<-0
for(i in 1:KO){
  for(j in 1:KO){
    if(i<j){
      K<-K+1
      #print(sprintf("(%d,%d) *",i,j),quote=0)
    }else{
      #print(sprintf("(%d,%d) ",i,j),quote=0)      
    }
  }
}
N<-KO^2
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
