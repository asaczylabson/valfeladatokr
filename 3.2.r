rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 #ismétlések száma
KO<-6 #"kocka" oldalainak a száma
KSZ<-3 #kockák száma
#itt 18-ig felsoroljuk a prímeket (szita):
iP<-c(0,1,1, 0,1,0, 1,0,0, 0,1,0, 1,0,0, 0,1,0)
K<-0#kedvező esetek száma
for(i in 1:ISM){
  dobas<-sample.int(KO,KSZ,replace=TRUE)
  if(iP[sum(dobas)]==1){K<-K+1}
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm -------------------->\n")
res<-rep(1,KO)
for(i in 2:KSZ){
  mmin<-i*1
  mmax<-i*KO
  newres<-rep(0,mmax)
  for(j in mmin:mmax){
    s<-0
    for(k in 1:KO){
      if(j-k>0&&j-k<=mmax-KO){s<-s+res[j-k]}
    }
    newres[j]<-s
  }
  res<-newres
}

#itt 18-ig felsoroljuk a prímeket (szita):
iP<-c(0,1,1, 0,1,0, 1,0,0, 0,1,0, 1,0,0, 0,1,0)
print(res)
K<-sum(res*iP)
N<-KO^KSZ
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)