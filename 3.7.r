rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 #ismétlések száma
y<-rep(0,ISM) #rajzolashoz
N0<-5 # mondjuk ezek a hölgyek
N1<-5
URNA<-c(rep(0,N0),rep(1,N1)) # 
K<-0

i<-0
while(i<ISM){
  i<-i+1
  huzas<-sample(URNA,N0+N1,replace=FALSE)
  huzas<-c(huzas,huzas[1])
  jo<-1
  j<-0
  while(j<N0+N1&&jo>0){
    j<-j+1
    if(huzas[j]==0&&huzas[j+1]==0){
      jo<-0
    }
  }
  # print(i)
  # print(huzas)
  # print(jo)
  if(jo==1){K<-K+1}
  y[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
plot(y,type="p",ylim=c(0,1))
cat("elm -------------------->\n")
source("nfakt.r")
K=nfakt(N0-1)*nfakt(N1)
N=nfakt(N0+N1-1)
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
