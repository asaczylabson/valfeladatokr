rm(list=ls())

cat("sim--------------------\n")
ISM<-10000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet
b1<-c(rep(0,5),rep(10,3),rep(50,2))
b2<-c(rep(0,2),rep(10,7),rep(50,1))
b3<-c(rep(0,1))
beg<-c(1,1+length(b1),1+length(b1)+length(b2))
end<-c(length(b1),length(b1)+length(b2),length(b1)+length(b2)+length(b3))

URNA<-c(b1,b2,b3)

K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  i<-i+1
  r<-sample.int(3,1)
  x<-sample(URNA[beg[r]:end[r]],1)
  if(x==50){
      K<-K+1
  }
    sim[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
#figyeljük meg hogy a sorrendet figyelembe vettük
p<-1/3*2/10+1/3*1/10+1/3*0 # teljes valség tétele 
print(sprintf("p=%f",p),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(p,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # base
g<-g+geom_line(aes(y=elm), colour="red") # layer 1
g<-g+geom_line(aes(y=sim), colour="green")  # layer 2
plot(g)


