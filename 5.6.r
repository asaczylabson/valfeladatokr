rm(list=ls())

cat("sim--------------------\n")
ISM<-5000 # ismétlések száma, 
sim<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
# ennyiszer végezzük el a kísérletet

K<-0 # kedvező esetek száma
i<-0
while(i<ISM){
  r<-sample(6,100,replace=TRUE) # 
  h<-0
  while(h<100){
    h<-h+1
    if(r[h]==6){
      if(h%%2==0){
        i<-i+1
        if(h==2){
          K<-K+1
        }
        sim[i]<-K/i
      }
      break
    }
  }
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm--------------------\n")
K<-5/36 # első nem hatos második hatos
N<-5/11 # mértani sor
print(sprintf("K=%f, N=%f, K/N=%f",K,N,K/N),quote=0)

library(ggplot2)
require(ggplot2)
x<-1:ISM
elm<-rep(K/N,ISM)
df<-data.frame(x,elm,sim)
g<-ggplot(df, aes(x)) # basic graphical object
g<-g+geom_line(aes(y=elm), colour="red") # first layer
g<-g+geom_line(aes(y=sim), colour="green")  # second layer
plot(g)


