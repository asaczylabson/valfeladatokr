rm(list=ls())
def.off()
cat("sim -------------------->\n")
ISM<-1000 #ismétlések száma
y<-rep(0,ISM) # rajzolashoz, a relatív gyakoriságok
KO<-6 #"kocka" oldalainak a száma
KSZ<-2 #kockák száma
CEL=8 #azt az összeget figyeljük
#6,2,8 -> 5/36
K<-0
i<-0
while(i<ISM){
  i<-i+1
  dobas<-sample.int(KO,KSZ,replace=TRUE)
  if(sum(dobas)==CEL){K<-K+1}
  y[i]<-K/i
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm -------------------->\n")
K<-0
for(i in 1:KO){
  for(j in 1:KO){
    if((i+j)==CEL){
      K<-K+1
      print(sprintf("(%d,%d) *",i,j),quote=0)
    }else{
      print(sprintf("(%d,%d) ",i,j),quote=0)      
    }
  }
}
N=KO^KSZ
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
x<-1:ISM
lines(x,y,type="p",ylim=c(0,1))
lines(x,rep(K/N,ISM),ylim=c(0,1), col="green")
