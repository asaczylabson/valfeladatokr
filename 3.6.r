rm(list=ls())

cat("sim -------------------->\n")
ISM<-1000 #ismétlések száma
URNA<-10 #"kocka" oldalainak a száma
CEL<-1
K<-0
for(i in 1:ISM){
  dobas<-sample.int(URNA,URNA,replace=FALSE)
  if(dobas[CEL]==CEL){K<-K+1}
}
print(sprintf("K=%d, ISM=%d, K/ISM=%f",K,ISM,K/ISM),quote=0)
cat("elm -------------------->\n")
source("nfakt.r")
K=nfakt(URNA-1)
N=nfakt(URNA)
print(sprintf("K=%d, N=%d, K/N=%f",K,N,K/N),quote=0)
